#!/bin/bash
TIME_NOW=`date +%Y/%m/%d-%H:%M:%S`
BUILD_DATE=`date +%Y_%m_%d`
BUILD_TAG=`date +%Y%m%d_%H%M%S`
IMAGE="buiducviet/node-project"
echo "Build docker image"
docker build -t buiducviet/node-project:$BUILD_TAG .

echo "Push image to registry"
docker push ${IMAGE}:${BUILD_TAG}
sleep 30
echo "Update and run new image on k8s cluster"
kubectl --kubeconfig /var/lib/jenkins/vain.config -n prod set image deployment shark-web-deployment shark-web=${IMAGE}:${BUILD_TAG} --record