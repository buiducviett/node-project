const express = require('express');
const app = express();
const router = express.Router();
let fs = require("fs");
let formidable = require("formidable");
 

const path = __dirname + '/views/';
const port = 3000;

router.use(function (req,res,next) {
  console.log('/' + req.method);
  next();
});

router.get('/', function(req,res){
  res.sendFile(path + 'index.html');
});

router.get('/sharks', function(req,res){
  res.sendFile(path + 'sharks.html');
});

router.get('/uploads', function(req,res){
  res.sendFile(path + 'upload.html');
});


app.use(express.static(path));
app.use('/', router);

app.listen(port, function () {
  console.log('Example app listening on port 3000!')
})
